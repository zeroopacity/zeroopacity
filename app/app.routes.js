define(function () {
    var routes = function ($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', {
        templateUrl: "app/components/home/Home.htm",
        controller: "homeController",
        data: {
            requireLogin: true
        }
    })
    .when('/registration', {
        templateUrl: "app/components/registration/Registration.htm",
        controller: "registrationController",
    })
    .otherwise({redirectTo:'/'});

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });
  };
  return ['$routeProvider', '$locationProvider', routes];
});