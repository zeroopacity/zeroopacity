define([
        'app/app.routes',
        'app/shared/constants/constants',
        'app/components/home/HomeController',
        'app/components/registration/RegistrationController'
    ],function (
        routes, 
        constants,  
        homeController,
        registrationController
    ) {

        var moduleDepedency = ['ngRoute'];

        if ( typeof DEBUG_ENVIROMENT === "undefined" ) {
            moduleDepedency.push('templates-main');
        }else {
            moduleDepedency.push('mockServiceModule');
        }       
        angular.module('ngAppBootsrapper', moduleDepedency)
            .config(routes)
            .constant("CONSTANTS", constants)
            .controller("homeController", homeController)
            .controller("registrationController", registrationController);

        angular.bootstrap(angular.element(document.querySelector('#ngApp')), ['ngAppBootsrapper']);
});